package com.richtree.rsf.client.mayurelectricals.Activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.richtree.rsf.client.mayurelectricals.Adapters.CategoryAdapter;
import com.richtree.rsf.client.mayurelectricals.Adapters.ProductAdapter;
import com.richtree.rsf.client.mayurelectricals.R;

public class Products extends AppCompatActivity {
    ListView listView;
    Button addcart;
    CardView increment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_products);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        addcart = (Button) findViewById(R.id.add_cart);
        increment =(CardView) findViewById(R.id.increment);

        listView = (ListView) findViewById(R.id.products);
        final ProductAdapter adapter=new ProductAdapter(this);
        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(Products.this, " " + adapter.names[i], Toast.LENGTH_SHORT).show();
            }
        });

        /*addcart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addcart.setVisibility(View.GONE);
                increment.setVisibility(View.VISIBLE);
            }
        });*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
