package com.richtree.rsf.client.mayurelectricals.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.richtree.rsf.client.mayurelectricals.R;

/**
 * Created by richtree on 8/2/2018.
 */

public class CategoryAdapter extends BaseAdapter {
    private Context c;

    public int[] Images = {R.drawable.type1, R.drawable.type2, R.drawable.type3, R.drawable.type4};
    public String[] names = {"Wires & Cables", "LED Bulbs", "MCB", "motors"};
    public String[] descriptions = {"Each application requires a certain wire size for installation.",
            "This listing features an Brand New, super bright LED Interior Package.",
            "A circuit breaker is an automatically operated electrical switch designed to protect.",
            "With our vast experience & knowledge in this field."};

    public CategoryAdapter(Context ctx) {
        this.c = ctx;
    }

    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int i) {
        return names[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        if (view == null) {

            LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.category_list, viewGroup, false);

            ImageView image=(ImageView)view.findViewById(R.id.category_image);
            TextView name = (TextView) view.findViewById(R.id.category_name);
            TextView description = (TextView)view.findViewById(R.id.category_description);

            image.setImageResource(Images[pos]);
            name.setText(names[pos]);
            description.setText(descriptions[pos]);

        }
        return view;
    }
}
