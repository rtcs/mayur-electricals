package com.richtree.rsf.client.mayurelectricals.Activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.richtree.rsf.client.mayurelectricals.R;

public class SignIn extends AppCompatActivity implements View.OnClickListener {
    TextView create, forgot;
    Button signin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        create = (TextView) findViewById(R.id.create_account);
        forgot = (TextView) findViewById(R.id.forgot_password);
        signin = (Button) findViewById(R.id.signin);

        create.setOnClickListener(this);
        forgot.setOnClickListener(this);
        signin.setOnClickListener(this);

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();

        switch (id) {
            case R.id.create_account:
                Intent create = new Intent(this, SignUp.class);
                startActivity(create);
                finish();
                break;
            case R.id.forgot_password:
                Intent forgot = new Intent(this, ForgotPassword.class);
                startActivity(forgot);
                finish();
                break;
            case R.id.signin:
                Intent signin = new Intent(this, MainActivity.class);
                startActivity(signin);
                finish();
                break;
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
