package com.richtree.rsf.client.mayurelectricals.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.richtree.rsf.client.mayurelectricals.R;

/**
 * Created by richtree on 8/4/2018.
 */

public class ProductAdapter extends BaseAdapter {
    private Context c;

    public int[] images = {R.drawable.product1, R.drawable.product2, R.drawable.product3, R.drawable.product4, R.drawable.product5, R.drawable.product6};
    public String[] names = {"Electric Fans", "Blenders", "Micro Waves", "Refrigerators", "Rice Cooker", "Solar Panel"};
    public String[] prices = {"2000$", "1500$", "3500$", "18000$", "1600$", "20000$"};

    public ProductAdapter(Context ctx) {
        this.c = ctx;
    }


    @Override
    public int getCount() {
        return names.length;
    }

    @Override
    public Object getItem(int i) {
        return names[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        if (view == null) {

            LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.product_list, viewGroup, false);

            ImageView image=(ImageView)view.findViewById(R.id.product_image);
            TextView name = (TextView) view.findViewById(R.id.product_name);
            TextView description = (TextView)view.findViewById(R.id.product_price);

            image.setImageResource(images[pos]);
            name.setText(names[pos]);
            description.setText(prices[pos]);

        }
        return view;
    }
}
