package com.richtree.rsf.client.mayurelectricals.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.richtree.rsf.client.mayurelectricals.R;


public class MyOrdersAdapter extends BaseAdapter {
    private Context c;

    public String[] o_id = {"ME002", "ME001"};
    public String[] c_name = {"Rajkumar", "Srinivas"};
    public String[] m_number = {"9533343303", "9550950504"};
    public String[] o_total = {"$ 14000", "$ 25300"};
    public String[] o_date = {"08-05-2018", "15-10-2018"};
    public String[] d_date = {"12-04-2019", "15-01-2020"};
    public String[] o_status = {"Pending", "Completed"};

    public MyOrdersAdapter(Context ctx) {
        this.c = ctx;
    }

    @Override
    public int getCount() {
        return o_id.length;
    }

    @Override
    public Object getItem(int i) {
        return o_id[i];
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int pos, View view, ViewGroup viewGroup) {
        if (view == null) {

            LayoutInflater inflater = (LayoutInflater) c.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view = inflater.inflate(R.layout.myorders_item, viewGroup, false);

        }

        TextView id = (TextView) view.findViewById(R.id.order_id);
        TextView name = (TextView) view.findViewById(R.id.customer_name);
        TextView number = (TextView)view.findViewById(R.id.phone_number);
        TextView total = (TextView) view.findViewById(R.id.total);
        TextView odate = (TextView) view.findViewById(R.id.order_date);
        TextView ddate = (TextView) view.findViewById(R.id.delivery_date);
        TextView status = (TextView) view.findViewById(R.id.status);

        id.setText(o_id[pos]);
        name.setText(c_name[pos]);
        number.setText(m_number[pos]);
        total.setText(o_total[pos]);
        odate.setText(o_date[pos]);
        ddate.setText(d_date[pos]);
        status.setText(o_status[pos]);

        return view;
    }
}

